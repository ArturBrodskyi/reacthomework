import ModalBody from "./ModalBody";
import ModalClose from "./ModalClose";
import ModalFooter from "./ModalFooter";
import ModalHeader from "./ModalHeader";
import ModalImage from "./ModalImage";
import ModalWrapper from "./ModalWrapper";

const Modal = (props) => {
  const {
    headerCont,
    bodyCont,
    firstButtonText,
    secondButtonText,
    onFirstButtonClick,
    onSecondButtonClick,
    className,
    isModalOpen,
    onCloseClick
  } = props;
  const modalStyle = {
    display: isModalOpen ? 'flex' : 'none'
};

  if (className ==='delete-modal') {
    return (
        <div className={`modal ${className}`} style={modalStyle}>
          <ModalWrapper>
            <ModalHeader>
                <ModalImage></ModalImage>
              {headerCont}
              <ModalClose onClick={onCloseClick}/>
            </ModalHeader>
            <ModalBody>{bodyCont}</ModalBody>
            <ModalFooter
              firstText={firstButtonText}
              secondaryText={secondButtonText}
              firstClick={onFirstButtonClick}
              secondaryClick={onSecondButtonClick}
            ></ModalFooter>
          </ModalWrapper>
        </div>
      );
  } else {
    return (
        <div className={`modal ${className}`} style={modalStyle}>
          <ModalWrapper>
            <ModalHeader>
              {headerCont}
            </ModalHeader>
            <ModalClose onClick={onCloseClick} />
            <ModalBody>{bodyCont}</ModalBody>
            <ModalFooter
              firstText={firstButtonText}
              secondaryText={secondButtonText}
              firstClick={onFirstButtonClick}
              secondaryClick={onSecondButtonClick}
            ></ModalFooter>
          </ModalWrapper>
        </div>
      );
  }
  
};

export default Modal;

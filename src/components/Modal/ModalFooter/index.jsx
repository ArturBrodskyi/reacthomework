const ModalFooter = ({firstText, secondaryText, firstClick, secondaryClick}) =>{
    return(
        <div className="modal-footer">
 {firstText && firstClick && (
                <button className={`${firstText}-btn footer-btn`} onClick={firstClick}>{firstText}</button>
            )}
            {secondaryText && secondaryClick && (
                <button className={`${secondaryText}-btn footer-btn`} onClick={secondaryClick}>{secondaryText}</button>
            )}
        </div>
    )
}

export default ModalFooter;
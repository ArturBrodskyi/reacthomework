const ModalClose = ({onClick}) =>{
    return (
        <button className="modal-close" onClick={onClick}>X</button>
    )
}
export default ModalClose;
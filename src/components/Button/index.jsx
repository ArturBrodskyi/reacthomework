const Button = (props) =>{
    const {type, className, onClick, children} = props
    return (
        <button className={className} onClick={onClick}>{children}</button>
    )
}

export default Button;
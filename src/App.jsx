import { useState } from 'react'
import Button from './components/Button'
import Modal from './components/Modal'
import './App.scss'

function App() {
  

  const [isFirstModalOpen, setIsFirstModalOpen] = useState(false);
  const [isSecondModalOpen, setIsSecondModalOpen] = useState(false);

  const openFirstModal = () => {
    setIsFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setIsFirstModalOpen(false);
  };

  const openSecondModal = () => {
    setIsSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setIsSecondModalOpen(false);
  };


  return (
    <>
          <div className={`overlay ${isFirstModalOpen || isSecondModalOpen ? 'active' : ''}`} onClick={() => {
        setIsFirstModalOpen(false);
        setIsSecondModalOpen(false);
      }}></div>

<Button onClick={openFirstModal} >Open first modal</Button>
<Button onClick={openSecondModal}>Open second modal</Button>



<Modal headerCont='Delete item?'
 bodyCont ='This will delete the item forever.
  Are you sure you want to continue?'
 firstButtonText='Delete'
 onFirstButtonClick={closeFirstModal}
 secondButtonText='Cancel'
 onSecondButtonClick={closeFirstModal}
 className = 'delete-modal'
 isModalOpen = {isFirstModalOpen}
 onCloseClick={closeFirstModal}
 ></Modal>

 <Modal headerCont='Add to favorites'
 bodyCont ='Add almond milk to your favorites?'
 firstButtonText='Add to favorites'
 onFirstButtonClick={closeSecondModal}
 className='fav-modal'
 isModalOpen={isSecondModalOpen}
 onCloseClick={closeSecondModal}
 ></Modal>
    </>
  )
}

export default App
